def extractAbstracts(content):
    ## Following SMJ format, isolate the abstracts:
    ## Terrible extraction of abstracts
    abstracts = [x.split("\t[")[0].split("\t")[-1] for x in content]
     
    ## Take out headers and footers- record those missed by the split
    missDex = []
    for i in range(len(abstracts)):
        abstracts[i] = abstracts[i].lower()
        if len(abstracts[i]) <100:
            missDex.append(i)
        if "copyright" in abstracts[i]:
            try:
                abstracts[i] = abstracts[i].split("copyright")[0]
            except IndexError:
                pass
        if "summary" in abstracts[i]:
            try:
                abstracts[i] = abstracts[i].split("research summary: ")[1]
                abstracts[i] = abstracts[i].split("research summary:")[1]
                abstracts[i] = abstracts[i].split("research summary")[1]
            except IndexError:
                continue
    print("Script extracted all but {} abstracts from {}".format(len(missDex), f.name))
    return abstracts


## Standard NLP box. ***No imports, please
def full_remove(x, removal_list):
    for char in removal_list:
        x = x.replace(char, ' ')
    return x
    
import string
def cleanAbstracts(abstracts,punctuation=string.punctuation):
    ## Remove leading and trailing white space
    abstracts = [x.strip() for x in abstracts]
     
    ## Remove digits
    digits = [str(x) for x in range(10)]
    digit_less = [full_remove(x, digits) for x in abstracts]
     
    ## Remove punctuation
    punc_less = [full_remove(x, list(punctuation)) for x in digit_less]
     
    ## Make everything lower-case
    abstracts_lower = [x.lower() for x in punc_less]
     
    ## Define our stop words ***Minimalist list first.
    stop_set = set(['the', 'a', 'an', 'i', 'he', 'she', 'they', 'to', 'of', 'it', 'from'])
     
    ## Remove stop words
    abstracts_split = [x.split() for x in abstracts_lower]
    abstracts_processed = [" ".join(list(filter(lambda a: a not in stop_set, x))) for x in abstracts_split]
     
    return abstracts_processed

#vocab = np.array([z[0] for z in sorted(vectorizer.vocabulary_.items(), key=lambda x:x[1])])
#
### Get indices of sorting w
#inds = np.argsort(w)
#
### Words with large negative values
#neg_inds = inds[0:50]
#print("Highly negative words: ")
#print([str(x) for x in list(vocab[neg_inds])])
#
### Words with large positive values
#pos_inds = inds[-49:-1]
#print("Highly positive words: ")
#print([str(x) for x in list(vocab[pos_inds])])