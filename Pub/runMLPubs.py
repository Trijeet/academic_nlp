## HB Perceptron for kernel v after SVM: https://gist.github.com/mblondel/656147 ***do we even want this?
def train_multiclass_perceptron(x,y,k,n_iters=100):
   n,d = x.shape
   w = np.zeros((k,d))
   b = np.zeros(k)
   done = False
   converged = True
   iters = 0
   np.random.seed(None)
   while not(done):
       done = True
       I = np.random.permutation(n)
       for j in I:
           pred_y = evaluate_classifier(w,b,x[j,:])
           true_y = int(y[j])
           if pred_y != true_y:
               w[true_y,:] = w[true_y,:] + x[j,:]
               b[true_y] = b[true_y] + 1.0
               w[pred_y,:] = w[pred_y,:] - x[j,:]
               b[pred_y] = b[pred_y] - 1.0
               done = False
       iters = iters + 1
       if iters > n_iters:
           done = True
           converged = False
   if converged:
       print ("Perceptron algorithm: iterations until convergence: ", iters)
   else:
       print ("Perceptron algorithm: did not converge within the specified number of iterations")
   return w, b, converged


import sklearn.svm
def fit_hinge_svm(C_value=1.0):
   clf = svm.LinearSVC(C=C_value, loss='hinge')
   clf.fit(train_data,train_labels)
   ## Get predictions on training data
   train_preds = clf.predict(train_data)
   train_error = float(np.sum((train_preds) != (train_labels)))/len(train_labels)
   ## Get predictions on test data
   test_preds = clf.predict(test_data)
   test_error = float(np.sum((test_preds) != (test_labels)))/len(test_labels)
   ##
   return train_error, test_error


def fit_crammer_singer_svm(C_value=1.0):
   clf = svm.LinearSVC(loss='hinge', multi_class='crammer_singer', C=C_value)
   clf.fit(train_data,train_labels)
   ## Get predictions on training data
   train_preds = clf.predict(train_data)
   train_error = float(np.sum((train_preds) != (train_labels)))/len(train_labels)
   ## Get predictions on test data
   test_preds = clf.predict(test_data)
   test_error = float(np.sum((test_preds) != (test_labels)))/len(test_labels)
   ##
   return train_error, test_error

def learn_kernel_SVM(datafile, kernel_type='rbf', C_value=1.0, s_value=1.0):
   data = np.loadtxt(datafile)
   n,d = data.shape
   # Create training set x and labels y
   x = data[:,0:2]
   y = data[:,2]
   # Now train a support vector machine and identify the support vectors
   if kernel_type == 'rbf':
       clf = SVC(kernel='rbf', C=C_value, gamma=1.0/(s_value*s_value))
   if kernel_type == 'quadratic':
       clf = SVC(kernel='poly', degree=2, C=C_value, coef0=1.0)
   clf.fit(x,y)
   sv = np.zeros(n,dtype=bool)
   sv[clf.support_] = True
   notsv = np.logical_not(sv)
   return clf, sv, notsv



## Homebrewed validation and parameter selection
def cross_validation_error(x,y,C_value,k):
   n = len(y)
   ## Randomly shuffle indices
   indices = np.random.permutation(n)
   
   ## Initialize error
   err = 0.0
   
   ## Iterate over partitions
   for i in range(k):
       ## Partition indices
       test_indices = indices[int(i*(n/k)):int((i+1)*(n/k) - 1)]
       train_indices = np.setdiff1d(indices, test_indices)
       
       ## Train classifier with parameter c ***link to choose_parameter() after its update
       clf = svm.LinearSVC(C=C_value, loss='hinge')
       clf.fit(x[train_indices], y[train_indices])
       
       ## Get predictions on test partition
       preds = clf.predict(x[test_indices])
       
       ## Compute error
       err += float(np.sum((preds > 0.0) != (y[test_indices] > 0.0)))/len(test_indices)
       
   return err/k
 
def choose_parameter(x,y,k):
     # Choose from C-candidates, geometrically at first ***Update to fix after approx 100 trials
   cvals = [0.01,0.1,1.0,10.0,100.0,1000.0,10000.0]
   wintest = 1
   for c in cvals:
       test_error = cross_validation_error(x,y,c,k)
       print ("Error rate for C = %0.2f: train %0.3f test %0.3f" % (c, train_error, test_error))
       if test_error<wintest:
           wintest=test_error
           winner = c
   
   return winner, wintest